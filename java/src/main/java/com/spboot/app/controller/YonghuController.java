package com.spboot.app.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.jntoo.db.DB;
import com.jntoo.db.utils.StringUtil;
import com.spboot.app.mapper.UserAddressMapper;
import com.spboot.app.mapper.YonghuMapper;
import com.spboot.app.pojo.ShoppingCart;
import com.spboot.app.pojo.ShoppingCartVo;
import com.spboot.app.pojo.UserAddress;
import com.spboot.app.pojo.Yonghu;
import com.spboot.app.service.YonghuService;
import com.spboot.app.utils.R;
import com.spboot.app.utils.SessionFactory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

@Api(tags = { "用户控制器" })
@RestController
@RequestMapping("/api/yonghu")
public class YonghuController {

    @Autowired
    public YonghuService yonghuService;

    @ApiOperation(value = "获取全部用户", httpMethod = "GET")
    @RequestMapping("/selectAll")
    public R<List<Yonghu>> selectAll() {
        return yonghuService.selectAll();
    }

    @ApiOperation(value = "根据条件筛选获取管理员列表，并分页", httpMethod = "POST")
    @RequestMapping("/selectPages")
    public R selectPages(@RequestBody Map<String, Object> req) {
        return yonghuService.selectPages(req);
    }

    @ApiOperation(value = "根据id获取信息", httpMethod = "GET")
    @RequestMapping("/findById")
    @ApiImplicitParam(name = "id", value = "用户对应的id", dataType = "Integer")
    public R findById(@RequestParam Integer id) {
        return yonghuService.findById(id);
    }

    @ApiOperation(value = "根据id更新数据", httpMethod = "POST")
    @RequestMapping("/update")
    @ApiImplicitParam(name = "data", value = "使用json数据提交", type = "json", dataTypeClass = Yonghu.class, paramType = "body")
    public R update(@RequestBody Map data) {
        Yonghu post = BeanUtil.mapToBean(data, Yonghu.class, true);
        return yonghuService.update(post, data);
    }

    @ApiOperation(value = "插入一行数据，返回插入后的点赞", httpMethod = "POST")
    @RequestMapping("/insert")
    @ApiImplicitParam(name = "data", value = "使用json数据提交", type = "json", dataTypeClass = Yonghu.class, paramType = "body")
    public R insert(@RequestBody Map data) {
        Yonghu post = BeanUtil.mapToBean(data, Yonghu.class, true);
        return yonghuService.insert(post, data);
    }

    @ApiOperation(value = "根据id列表删除数据", httpMethod = "POST")
    @RequestMapping("/delete")
    @ApiImplicitParam(name = "id", value = "用户对应的id", type = "json", dataTypeClass = List.class)
    public R delete(@RequestBody List<Integer> id) {
        return yonghuService.delete(id);
    }

    @ApiOperation(value = "保存收货地址", httpMethod = "POST")
    @RequestMapping("/saveUserAddress")
    public R saveUserAddress(String userId,String address,String mobile,String names,Integer isDefault){ // todo isDefault 是否默认地址 0 否 1 是
        try {
            if (Objects.isNull(isDefault)){
                isDefault = 0;
            }
            if (StringUtils.isEmpty(mobile) ||  StringUtils.isEmpty(userId) || StringUtils.isEmpty(address) || StringUtils.isEmpty(names) ){
                return R.error("参数异常,请填写信息..");
            }
            yonghuService.saveUserAddress(userId,address,mobile,names,isDefault);
            return R.ok();
        }catch (Exception e){
            return R.error("保存失败");
        }
    }

    @ApiOperation(value = "根据收货地址主键查询详情", httpMethod = "POST")
    @RequestMapping("/getUserAddressById")
    public R getUserAddressById(String id){
        try {
            UserAddress userAddress = yonghuService.getUserAddressById(id);
            return R.success(userAddress);
        }catch (Exception e){
            return R.error("查询失败");
        }
    }

    @ApiOperation(value = "修改收货地址", httpMethod = "POST")
    @RequestMapping("/updateUserAddress")
    public R updateUserAddress(String id,String address,String mobile,String names,Integer isDefault){
        try {
            if (Objects.isNull(isDefault)){
                isDefault = 0;
            }
            if (StringUtils.isEmpty(mobile) ||  StringUtils.isEmpty(id) || StringUtils.isEmpty(address) || StringUtils.isEmpty(names) ){
                return R.error("参数异常");
            }
            yonghuService.updateUserAddress(id,address,mobile,names,isDefault);
            return R.ok();
        }catch (Exception e){
            return R.error("保存失败");
        }
    }

    @ApiOperation(value = "根据用户id查询默认收货地址", httpMethod = "POST")
    @RequestMapping("/getDefaultUserAddress")
    public R getDefaultUserAddress(String userId){
        try {
            UserAddress userAddress = yonghuService.getDefaultUserAddress(userId);
            return R.success(userAddress);
        }catch (Exception e){
            return R.error("查询失败");
        }
    }

    @ApiOperation(value = "根据用户id查询收货地址列表", httpMethod = "POST")
    @RequestMapping("/listUserAddress")
    public R listUserAddress(String userId){
        try {
            List<UserAddress> list = yonghuService.listUserAddress(userId);
            if (!CollectionUtils.isEmpty(list)){
                list = list.stream()
                        .sorted(Comparator.comparingInt(UserAddress::getIsDefault).reversed())
                        .collect(Collectors.toList());
            }
            return R.success(list);
        }catch (Exception e){
            return R.error("查询失败");
        }
    }

    @ApiOperation(value = "根据主键删除收货地址", httpMethod = "POST")
    @RequestMapping("/delUserAddressById")
    public R delUserAddressById(String id){
        try {
            yonghuService.delUserAddressById(id);
            return R.ok();
        }catch (Exception e){
            return R.error("操作失败");
        }
    }

    @ApiOperation(value = "添加购物车", httpMethod = "POST")
    @RequestMapping("/saveShoppingCard")
    public R saveShoppingCard(@RequestBody ShoppingCart shoppingCart){
        try {
            if (Objects.isNull(shoppingCart)){
                throw new RuntimeException("参数异常");
            }
            if (Objects.isNull(shoppingCart.getUserId())){
                throw new RuntimeException("用户id不能为空");
            }
            if (Objects.isNull(shoppingCart.getItemId())){
                throw new RuntimeException("商品id不能为空");
            }
            if (Objects.isNull(shoppingCart.getNumbers()) || shoppingCart.getNumbers() < 1){
                throw new RuntimeException("数量不能为空");
            }
            yonghuService.saveShoppingCard(shoppingCart);
            return R.ok();
        }catch (Exception e){
            return R.error("添加购物车失败");
        }
    }


    @ApiOperation(value = "根据用户id查询购物车列表", httpMethod = "POST")
    @RequestMapping("/listShoppingCard")
    public R listShoppingCard(String userId){
        try {
            List<ShoppingCartVo> list = yonghuService.listShoppingCard(userId);
            return R.success(list);
        }catch (Exception e){
            return R.error("查询失败");
        }
    }

    @ApiOperation(value = "购物车 增加/减少 商品数量", httpMethod = "POST")
    @RequestMapping("/updateItemNumber")
    public R updateItemNumber(String id,Integer numbers){
        try {
            yonghuService.updateItemNumber(id,numbers);
            return R.ok();
        }catch (Exception e){
            return R.error("操作失败");
        }
    }

    @ApiOperation(value = "清空购物车", httpMethod = "POST")
    @RequestMapping("/clear")
    public R clear(String userId){
        try {
            yonghuService.clear(userId);
            return R.ok();
        }catch (Exception e){
            return R.error("操作失败");
        }
    }

    @ApiOperation(value = "根据主键删除购物车商品", httpMethod = "POST")
    @RequestMapping("/delItem")
    public R delItem(String id){
        try {
            yonghuService.delItem(id);
            return R.ok();
        }catch (Exception e){
            return R.error("操作失败");
        }
    }

}
