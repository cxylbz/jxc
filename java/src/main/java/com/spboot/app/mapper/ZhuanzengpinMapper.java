package com.spboot.app.mapper;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.spboot.app.pojo.Zhuanzengpin;
import org.apache.ibatis.annotations.Mapper;

@Mapper
@TableName("转赠品")
public interface ZhuanzengpinMapper extends BaseMapper<Zhuanzengpin> {}
