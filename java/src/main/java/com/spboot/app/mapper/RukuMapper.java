package com.spboot.app.mapper;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.spboot.app.pojo.Ruku;
import org.apache.ibatis.annotations.Mapper;

@Mapper
@TableName("入库")
public interface RukuMapper extends BaseMapper<Ruku> {}
