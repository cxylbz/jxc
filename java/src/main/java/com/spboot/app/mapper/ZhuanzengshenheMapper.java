package com.spboot.app.mapper;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.spboot.app.pojo.Zhuanzengshenhe;
import org.apache.ibatis.annotations.Mapper;

@Mapper
@TableName("转赠审核")
public interface ZhuanzengshenheMapper extends BaseMapper<Zhuanzengshenhe> {}
