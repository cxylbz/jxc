package com.spboot.app.mapper;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.spboot.app.pojo.Tuihuanhuo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
@TableName("退换货")
public interface TuihuanhuoMapper extends BaseMapper<Tuihuanhuo> {}
