package com.spboot.app.mapper;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.spboot.app.pojo.Zhuanshangpin;
import org.apache.ibatis.annotations.Mapper;

@Mapper
@TableName("转商品")
public interface ZhuanshangpinMapper extends BaseMapper<Zhuanshangpin> {}
