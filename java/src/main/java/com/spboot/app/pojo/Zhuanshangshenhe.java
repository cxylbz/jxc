package com.spboot.app.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jntoo.db.DB;
import java.io.Serializable;
import java.util.*;

@TableName("zhuanshangshenhe")
public class Zhuanshangshenhe implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer zhuanshangpinid;

    private Integer zengpinid;

    private String bianhao;

    private String mingcheng;

    private Integer fenlei;

    private Integer kucun;

    private String shenqingren;

    private String shenhejieguo;

    private String shenhebeizhu;

    private String shenheren;

    private String addtime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getZhuanshangpinid() {
        return zhuanshangpinid;
    }

    public void setZhuanshangpinid(Integer zhuanshangpinid) {
        this.zhuanshangpinid = zhuanshangpinid == null ? 0 : zhuanshangpinid;
    }

    public Integer getZengpinid() {
        return zengpinid;
    }

    public void setZengpinid(Integer zengpinid) {
        this.zengpinid = zengpinid == null ? 0 : zengpinid;
    }

    public String getBianhao() {
        return bianhao;
    }

    public void setBianhao(String bianhao) {
        this.bianhao = bianhao == null ? "" : bianhao.trim();
    }

    public String getMingcheng() {
        return mingcheng;
    }

    public void setMingcheng(String mingcheng) {
        this.mingcheng = mingcheng == null ? "" : mingcheng.trim();
    }

    public Integer getFenlei() {
        return fenlei;
    }

    public void setFenlei(Integer fenlei) {
        this.fenlei = fenlei == null ? 0 : fenlei;
    }

    public Integer getKucun() {
        return kucun;
    }

    public void setKucun(Integer kucun) {
        this.kucun = kucun == null ? 0 : kucun;
    }

    public String getShenqingren() {
        return shenqingren;
    }

    public void setShenqingren(String shenqingren) {
        this.shenqingren = shenqingren == null ? "" : shenqingren.trim();
    }

    public String getShenhejieguo() {
        return shenhejieguo;
    }

    public void setShenhejieguo(String shenhejieguo) {
        this.shenhejieguo = shenhejieguo == null ? "" : shenhejieguo.trim();
    }

    public String getShenhebeizhu() {
        return shenhebeizhu;
    }

    public void setShenhebeizhu(String shenhebeizhu) {
        this.shenhebeizhu = shenhebeizhu == null ? "" : shenhebeizhu.trim();
    }

    public String getShenheren() {
        return shenheren;
    }

    public void setShenheren(String shenheren) {
        this.shenheren = shenheren == null ? "" : shenheren.trim();
    }

    public String getAddtime() {
        return addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime == null ? "" : addtime.trim();
    }
}
