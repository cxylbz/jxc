package com.spboot.app.mapper;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.spboot.app.pojo.Zhuanshangshenhe;
import org.apache.ibatis.annotations.Mapper;

@Mapper
@TableName("转商审核")
public interface ZhuanshangshenheMapper extends BaseMapper<Zhuanshangshenhe> {}
