package com.spboot.app.mapper;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.spboot.app.pojo.Zengpin;
import org.apache.ibatis.annotations.Mapper;

@Mapper
@TableName("赠品")
public interface ZengpinMapper extends BaseMapper<Zengpin> {}
