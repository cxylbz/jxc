import http from "@/utils/ajax/http";
import { useRoute } from "vue-router";
import { useUserStore } from "@/stores";
import { reactive, ref, unref } from "vue";
import rule from "@/utils/rule";
import { extend, isArray } from "@/utils/extend";
import { ElMessageBox } from "element-plus";
import router from "@/router";

import { canZengpinFindById } from "./zengpin";

/**
 * 响应式的对象数据
 */

export const ZhuanshangpinCreateForm = () => {
    var route = unref(router.currentRoute);
    const userStore = useUserStore();
    const $session = userStore.session;
    if (!route.query) {
        route = useRoute();
    }
    const form = {
        bianhao: "",
        mingcheng: "",
        fenlei: "",
        kucun: "",
        zhuanshangmiaoshu: "",
        zhuanshangzhuangtai: "待审核",
        shenqingren: $session.username,
    };

    return form;
};

function exportForm(form, readMap) {
    var autoText = ["zengpinid", "bianhao", "mingcheng", "fenlei", "kucun"];
    for (var txt of autoText) {
        form[txt] = readMap[txt];
    }
}

/**
 * 异步模式获取数据
 * @param id
 * @param readMap
 * @return {Promise}
 */
export const canZhuanshangpinCreateForm = (id, readMap) => {
    return new Promise(async (resolve, reject) => {
        var form = ZhuanshangpinCreateForm();
        if (!readMap || !readMap.id) {
            readMap = await canZengpinFindById(id).catch(reject);
        }
        exportForm(form, readMap);
        form.zengpinid = readMap.id;
        resolve({ form, readMap });
    });
};

/**
 * 响应式获取转商品 模块的表单字段数据
 * @return {UnwrapNestedRefs<{}>}
 */
export const useZhuanshangpinCreateForm = (id) => {
    const form = ZhuanshangpinCreateForm();
    const formReactive = reactive(form);

    const readMap = reactive({});
    canZengpinFindById(id).then(
        (map) => {
            exportForm(formReactive, map);
            extend(readMap, map);
            formReactive.zengpinid = map.id;
        },
        (err) => {
            ElMessageBox.alert(err.message);
        }
    );
    return { form: formReactive, readMap };
};

export const canZhuanshangpinSelect = (filter, result) => {
    http.post("/api/zhuanshangpin/selectPages").then((res) => {
        if (res.code == 0) {
            extend(result, res.data);
        } else {
            ElMessageBox.alert(res.msg);
        }
    });
};

/**
 * 获取分页数据
 * @param filter
 */
export const useZhuanshangpinSelect = (filter) => {
    const result = reactive({
        lists: [],
        total: {},
    });
    canZhuanshangpinSelect(filter, result);
    return result;
};

/**
 * 根据
 * @param id
 * @return {Promise|form}
 */
export const canZhuanshangpinFindById = (id) => {
    return new Promise((resolve, reject) => {
        // 读取后台数据
        http.get("/api/zhuanshangpin/findById", { id }).then((res) => {
            if (res.code == 0) {
                resolve(res.data);
            } else {
                reject(new Error(res.msg));
            }
        }, reject);
    });
};

/**
 * 根据id 获取一行数据
 * @param id
 * @return {UnwrapNestedRefs<{}>}
 */
export const useZhuanshangpinFindById = (id) => {
    var form = reactive({});

    canZhuanshangpinFindById(id).then((res) => {
        extend(form, res);
    });
    return form;
};

/**
 * 根据数据,插入到数据库中
 * @param data
 * @return {Promise<unknown>}
 */
export const canZhuanshangpinInsert = (data) => {
    return new Promise((resolve, reject) => {
        http.post("/api/zhuanshangpin/insert", data)
            .json()
            .then(
                (res) => {
                    resolve(res);
                },
                (err) => {
                    reject(err);
                }
            );
    });
};

/**
 * 根据数据更新数据库
 * @param data
 * @return {Promise<unknown>}
 */
export const canZhuanshangpinUpdate = (data) => {
    return new Promise((resolve, reject) => {
        http.post("/api/zhuanshangpin/update", data)
            .json()
            .then(
                (res) => {
                    resolve(res);
                },
                (err) => {
                    reject(err);
                }
            );
    });
};

/**
 * 根据id 或者列表id
 * @param id
 * @return {Promise<unknown>}
 */
export const canZhuanshangpinDelete = (id) => {
    var res = [];
    if (!isArray(id)) {
        res.push(id);
    } else {
        res = id;
    }

    return new Promise((resolve, reject) => {
        http.post("/api/zhuanshangpin/delete", res)
            .json()
            .then(
                (res) => {
                    resolve(res);
                },
                (err) => {
                    reject(err);
                }
            );
    });
};
