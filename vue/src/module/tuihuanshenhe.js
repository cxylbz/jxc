import http from "@/utils/ajax/http";
import { useRoute } from "vue-router";
import { useUserStore } from "@/stores";
import { reactive, ref, unref } from "vue";
import rule from "@/utils/rule";
import { extend, isArray } from "@/utils/extend";
import { ElMessageBox } from "element-plus";
import router from "@/router";

import { canTuihuanhuoFindById } from "./tuihuanhuo";

/**
 * 响应式的对象数据
 */

export const TuihuanshenheCreateForm = () => {
    var route = unref(router.currentRoute);
    const userStore = useUserStore();
    const $session = userStore.session;
    if (!route.query) {
        route = useRoute();
    }
    const form = {
        bianhao: "",
        mingcheng: "",
        fenlei: "",
        jiage: "",
        tupian: "",
        shuliang: "",
        xingming: "",
        lianxifangshi: "",
        dizhi: "",
        leixing: "",
        caozuoren: $session.username,
        tianjiaren: $session.username,
        shenhejieguo: "",
        shenhebeizhu: "",
        shenheren: $session.username,
    };

    return form;
};

function exportForm(form, readMap) {
    var autoText = [
        "tuihuanhuoid",
        "dingdanid",
        "shangpinid",
        "bianhao",
        "mingcheng",
        "fenlei",
        "jiage",
        "tupian",
        "shuliang",
        "zonge",
        "xingming",
        "lianxifangshi",
        "dizhi",
        "leixing",
        "caozuoren",
        "tianjiaren",
    ];
    for (var txt of autoText) {
        form[txt] = readMap[txt];
    }
}

/**
 * 异步模式获取数据
 * @param id
 * @param readMap
 * @return {Promise}
 */
export const canTuihuanshenheCreateForm = (id, readMap) => {
    return new Promise(async (resolve, reject) => {
        var form = TuihuanshenheCreateForm();
        if (!readMap || !readMap.id) {
            readMap = await canTuihuanhuoFindById(id).catch(reject);
        }
        exportForm(form, readMap);
        form.tuihuanhuoid = readMap.id;
        resolve({ form, readMap });
    });
};

/**
 * 响应式获取退换审核 模块的表单字段数据
 * @return {UnwrapNestedRefs<{}>}
 */
export const useTuihuanshenheCreateForm = (id) => {
    const form = TuihuanshenheCreateForm();
    const formReactive = reactive(form);

    const readMap = reactive({});
    canTuihuanhuoFindById(id).then(
        (map) => {
            exportForm(formReactive, map);
            extend(readMap, map);
            formReactive.tuihuanhuoid = map.id;
        },
        (err) => {
            ElMessageBox.alert(err.message);
        }
    );
    return { form: formReactive, readMap };
};

export const canTuihuanshenheSelect = (filter, result) => {
    http.post("/api/tuihuanshenhe/selectPages").then((res) => {
        if (res.code == 0) {
            extend(result, res.data);
        } else {
            ElMessageBox.alert(res.msg);
        }
    });
};

/**
 * 获取分页数据
 * @param filter
 */
export const useTuihuanshenheSelect = (filter) => {
    const result = reactive({
        lists: [],
        total: {},
    });
    canTuihuanshenheSelect(filter, result);
    return result;
};

/**
 * 根据
 * @param id
 * @return {Promise|form}
 */
export const canTuihuanshenheFindById = (id) => {
    return new Promise((resolve, reject) => {
        // 读取后台数据
        http.get("/api/tuihuanshenhe/findById", { id }).then((res) => {
            if (res.code == 0) {
                resolve(res.data);
            } else {
                reject(new Error(res.msg));
            }
        }, reject);
    });
};

/**
 * 根据id 获取一行数据
 * @param id
 * @return {UnwrapNestedRefs<{}>}
 */
export const useTuihuanshenheFindById = (id) => {
    var form = reactive({});

    canTuihuanshenheFindById(id).then((res) => {
        extend(form, res);
    });
    return form;
};

/**
 * 根据数据,插入到数据库中
 * @param data
 * @return {Promise<unknown>}
 */
export const canTuihuanshenheInsert = (data) => {
    return new Promise((resolve, reject) => {
        http.post("/api/tuihuanshenhe/insert", data)
            .json()
            .then(
                (res) => {
                    resolve(res);
                },
                (err) => {
                    reject(err);
                }
            );
    });
};

/**
 * 根据数据更新数据库
 * @param data
 * @return {Promise<unknown>}
 */
export const canTuihuanshenheUpdate = (data) => {
    return new Promise((resolve, reject) => {
        http.post("/api/tuihuanshenhe/update", data)
            .json()
            .then(
                (res) => {
                    resolve(res);
                },
                (err) => {
                    reject(err);
                }
            );
    });
};

/**
 * 根据id 或者列表id
 * @param id
 * @return {Promise<unknown>}
 */
export const canTuihuanshenheDelete = (id) => {
    var res = [];
    if (!isArray(id)) {
        res.push(id);
    } else {
        res = id;
    }

    return new Promise((resolve, reject) => {
        http.post("/api/tuihuanshenhe/delete", res)
            .json()
            .then(
                (res) => {
                    resolve(res);
                },
                (err) => {
                    reject(err);
                }
            );
    });
};
