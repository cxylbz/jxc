import http from "@/utils/ajax/http";
import { useRoute } from "vue-router";
import { useUserStore } from "@/stores";
import { reactive, ref, unref } from "vue";
import rule from "@/utils/rule";
import { extend, isArray } from "@/utils/extend";
import { ElMessageBox } from "element-plus";
import router from "@/router";

import { canDingdanFindById } from "./dingdan";

/**
 * 响应式的对象数据
 */

export const ChukuCreateForm = () => {
    var route = unref(router.currentRoute);
    const userStore = useUserStore();
    const $session = userStore.session;
    if (!route.query) {
        route = useRoute();
    }
    const form = {
        bianhao: "",
        mingcheng: "",
        fenlei: "",
        tupian: "",
        jiage: "",
        shuliang: "",
        xingming: "",
        lianxifangshi: "",
        dizhi: "",
        tianjiaren: $session.username,
        kuaidigongsi: "",
        kuaididanhao: "",
        chukubeizhu: "",
        caozuoren: $session.username,
    };

    return form;
};

function exportForm(form, readMap) {
    var autoText = ["dingdanid", "shangpinid", "bianhao", "mingcheng", "fenlei", "tupian", "jiage", "shuliang", "zonge", "xingming", "lianxifangshi", "dizhi", "tianjiaren"];
    for (var txt of autoText) {
        form[txt] = readMap[txt];
    }
}

/**
 * 异步模式获取数据
 * @param id
 * @param readMap
 * @return {Promise}
 */
export const canChukuCreateForm = (id, readMap) => {
    return new Promise(async (resolve, reject) => {
        var form = ChukuCreateForm();
        if (!readMap || !readMap.id) {
            readMap = await canDingdanFindById(id).catch(reject);
        }
        exportForm(form, readMap);
        form.dingdanid = readMap.id;
        resolve({ form, readMap });
    });
};

/**
 * 响应式获取出库 模块的表单字段数据
 * @return {UnwrapNestedRefs<{}>}
 */
export const useChukuCreateForm = (id) => {
    const form = ChukuCreateForm();
    const formReactive = reactive(form);

    const readMap = reactive({});
    canDingdanFindById(id).then(
        (map) => {
            exportForm(formReactive, map);
            extend(readMap, map);
            formReactive.dingdanid = map.id;
        },
        (err) => {
            ElMessageBox.alert(err.message);
        }
    );
    return { form: formReactive, readMap };
};

export const canChukuSelect = (filter, result) => {
    http.post("/api/chuku/selectPages").then((res) => {
        if (res.code == 0) {
            extend(result, res.data);
        } else {
            ElMessageBox.alert(res.msg);
        }
    });
};

/**
 * 获取分页数据
 * @param filter
 */
export const useChukuSelect = (filter) => {
    const result = reactive({
        lists: [],
        total: {},
    });
    canChukuSelect(filter, result);
    return result;
};

/**
 * 根据
 * @param id
 * @return {Promise|form}
 */
export const canChukuFindById = (id) => {
    return new Promise((resolve, reject) => {
        // 读取后台数据
        http.get("/api/chuku/findById", { id }).then((res) => {
            if (res.code == 0) {
                resolve(res.data);
            } else {
                reject(new Error(res.msg));
            }
        }, reject);
    });
};

/**
 * 根据id 获取一行数据
 * @param id
 * @return {UnwrapNestedRefs<{}>}
 */
export const useChukuFindById = (id) => {
    var form = reactive({});

    canChukuFindById(id).then((res) => {
        extend(form, res);
    });
    return form;
};

/**
 * 根据数据,插入到数据库中
 * @param data
 * @return {Promise<unknown>}
 */
export const canChukuInsert = (data) => {
    return new Promise((resolve, reject) => {
        http.post("/api/chuku/insert", data)
            .json()
            .then(
                (res) => {
                    resolve(res);
                },
                (err) => {
                    reject(err);
                }
            );
    });
};

/**
 * 根据数据更新数据库
 * @param data
 * @return {Promise<unknown>}
 */
export const canChukuUpdate = (data) => {
    return new Promise((resolve, reject) => {
        http.post("/api/chuku/update", data)
            .json()
            .then(
                (res) => {
                    resolve(res);
                },
                (err) => {
                    reject(err);
                }
            );
    });
};

/**
 * 根据id 或者列表id
 * @param id
 * @return {Promise<unknown>}
 */
export const canChukuDelete = (id) => {
    var res = [];
    if (!isArray(id)) {
        res.push(id);
    } else {
        res = id;
    }

    return new Promise((resolve, reject) => {
        http.post("/api/chuku/delete", res)
            .json()
            .then(
                (res) => {
                    resolve(res);
                },
                (err) => {
                    reject(err);
                }
            );
    });
};
