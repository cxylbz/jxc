package com.spboot.app.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jntoo.db.DB;
import java.io.Serializable;
import java.util.*;

@TableName("zhuanzengpin")
public class Zhuanzengpin implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer shangpinid;

    private String bianhao;

    private String mingcheng;

    private Integer fenlei;

    private Integer kucun;

    private Integer zhuanzengshangpin;

    private String zhuanzengzhuangtai;

    private String zhuanzengmiaoshu;

    private String shenqingren;

    private String addtime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getZhuanzengshenheCount() {
        return DB.name("zhuanzengshenhe").where("zhuanzengpinid", id).count();
    }

    public Integer getShangpinid() {
        return shangpinid;
    }

    public void setShangpinid(Integer shangpinid) {
        this.shangpinid = shangpinid == null ? 0 : shangpinid;
    }

    public String getBianhao() {
        return bianhao;
    }

    public void setBianhao(String bianhao) {
        this.bianhao = bianhao == null ? "" : bianhao.trim();
    }

    public String getMingcheng() {
        return mingcheng;
    }

    public void setMingcheng(String mingcheng) {
        this.mingcheng = mingcheng == null ? "" : mingcheng.trim();
    }

    public Integer getFenlei() {
        return fenlei;
    }

    public void setFenlei(Integer fenlei) {
        this.fenlei = fenlei == null ? 0 : fenlei;
    }

    public Integer getKucun() {
        return kucun;
    }

    public void setKucun(Integer kucun) {
        this.kucun = kucun == null ? 0 : kucun;
    }

    public Integer getZhuanzengshangpin() {
        return zhuanzengshangpin;
    }

    public void setZhuanzengshangpin(Integer zhuanzengshangpin) {
        this.zhuanzengshangpin = zhuanzengshangpin == null ? 0 : zhuanzengshangpin;
    }

    public String getZhuanzengzhuangtai() {
        return zhuanzengzhuangtai;
    }

    public void setZhuanzengzhuangtai(String zhuanzengzhuangtai) {
        this.zhuanzengzhuangtai = zhuanzengzhuangtai == null ? "" : zhuanzengzhuangtai.trim();
    }

    public String getZhuanzengmiaoshu() {
        return zhuanzengmiaoshu;
    }

    public void setZhuanzengmiaoshu(String zhuanzengmiaoshu) {
        this.zhuanzengmiaoshu = zhuanzengmiaoshu == null ? "" : zhuanzengmiaoshu.trim();
    }

    public String getShenqingren() {
        return shenqingren;
    }

    public void setShenqingren(String shenqingren) {
        this.shenqingren = shenqingren == null ? "" : shenqingren.trim();
    }

    public String getAddtime() {
        return addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime == null ? "" : addtime.trim();
    }
}
