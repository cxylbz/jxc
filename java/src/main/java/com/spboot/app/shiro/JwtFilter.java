package com.spboot.app.shiro;

import com.jntoo.db.utils.StringUtil;
import java.util.Map;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.spring.web.config.ShiroFilterChainDefinition;
import org.apache.shiro.web.filter.authc.BasicHttpAuthenticationFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMethod;

public class JwtFilter extends BasicHttpAuthenticationFilter {

    private static final Logger log = LoggerFactory.getLogger(JwtFilter.class);

    @Autowired
    private ShiroFilterChainDefinition shiroFilterChainDefinition;

    //private AntPathMatcher antPathMatcher = new AntPathMatcher();

    /**
     * 执行登录认证(判断请求头是否带上token)
     * @param request
     * @param response
     * @param mappedValue
     * @return
     */
    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        HttpServletRequest req = (HttpServletRequest) request;
        log.info("JwtFilter-->>>isAccessAllowed-Method:init(),{}", req.getRequestURI());
        //如果请求头不存在token,则可能是执行登陆操作或是游客状态访问,直接返回true
        if (isLoginAttempt(request, response)) {
            return true;
        }
        //如果存在,则进入executeLogin方法执行登入,检查token 是否正确
        try {
            executeLogin(request, response);
            return true;
        } catch (Exception e) {
            throw new AuthenticationException("Token失效请重新登录");
        }
    }

    /**
     * 判断用户是否是登入,检测headers里是否包含token字段
     */
    @Override
    protected boolean isLoginAttempt(ServletRequest request, ServletResponse response) {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        log.info("isLoginAttempt {}", appliedPaths.toString());
        log.info("JwtFilter-->>>isLoginAttempt-Method:init():{}", httpServletRequest.getRequestURI());
        HttpServletRequest req = (HttpServletRequest) request;
        if (pathsMatch("/login", request)) {
            return true;
        }
        Map<String, String> map = shiroFilterChainDefinition.getFilterChainMap();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            if (pathsMatch(entry.getKey(), httpServletRequest.getRequestURI())) {
                return true;
            }
        }

        String token = req.getHeader("token");
        if (StringUtil.isNullOrEmpty(token)) {
            return false;
        }
        log.info("JwtFilter-->>>isLoginAttempt-Method:返回true");
        return false;
    }

    /**
     * 重写AuthenticatingFilter的executeLogin方法丶执行登陆操作
     */
    @Override
    protected boolean executeLogin(ServletRequest request, ServletResponse response) throws Exception {
        log.info("JwtFilter-->>>executeLogin-Method:init()");
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        String token = httpServletRequest.getHeader("token"); //Access-Token
        JwtToken jwtToken = new JwtToken(token);
        // 提交给realm进行登入,如果错误他会抛出异常并被捕获, 反之则代表登入成功,返回true
        getSubject(request, response).login(jwtToken);
        return true;
    }

    /**
     * 对跨域提供支持
     */
    @Override
    protected boolean preHandle(ServletRequest req, ServletResponse rsp) throws Exception {
        log.info("JwtFilter-->>>preHandle-Method:init()");
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) rsp;
        if (!StringUtil.isNullOrEmpty(request.getHeader("Origin")) && response.getHeader("Access-Control-Allow-Origin") == null) {
            response.addHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
            response.addHeader("Access-Control-Allow-Methods", "POST, GET ,OPTIONS, PUT,DELETE");
            response.addHeader("Access-Control-Allow-Headers", "x-requested-with,token,content-type,auth-token,app-http,Authorization");
            response.addHeader("Access-Control-Allow-Credentials", "true");
            response.addHeader("Access-Control-Max-Age", Integer.toString(86400 * 7));

            if (request.getMethod().equals(RequestMethod.OPTIONS.name())) {
                if ("OPTIONS".equals(request.getMethod().toUpperCase())) { // 这个方法只是试探，所以需要直接返回信息
                    response.setStatus(200);
                    return false;
                }
            }
        }

        return super.preHandle(request, response);
    }
}
