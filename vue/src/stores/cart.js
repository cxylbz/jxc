import { defineStore } from "pinia";
import { ref, reactive } from "vue";
import http from "@/utils/ajax/http";

import { useUserStore } from "./user";
import { ElMessageBox } from "element-plus";

export const useCartStore = defineStore("cart", () => {

  const cartList = reactive([]);
  const loading = ref(false)

  const userStore = useUserStore();

  async function addCart(model) {
    try {
      const result = await http.post("/api/yonghu/saveShoppingCard", Object.assign({}, model, {
        userId: userStore.getSession("id"),
      }));
      if (result.code === 0) {
        return true
      }
      await ElMessageBox.alert(res.msg);
      return false;
    } catch (error) {
      return false;
    }
  }

  async function getCartList() {
    try {
      cartList.splice(0, cartList.length);
      // loading.value = true;
      const result = await http.post("/api/yonghu/listShoppingCard?userId=" + userStore.getSession("id"));
      if (result.code === 0) {
        cartList.push(...result.data);
        return true
      }
      await ElMessageBox.alert(res.msg);
      return false;
    } finally {
      loading.value = false;
    }
  }

  async function updateCart() {
  }

  async function updateItemNumber(id, numbers) {
    try {
      const result = await http.post("/api/yonghu/updateItemNumber?" + `id=${id}&numbers=${numbers}`,);
      if (result.code === 0) {
        return true
      }
      await ElMessageBox.alert(res.msg);
      return false;
    } catch (error) {
      return false;
    }
  }
  async function removeCartById(id) {
    try {
      const result = await http.post("/api/yonghu/delItem?" + `id=${id}`,);
      if (result.code === 0) {
        return true
      }
      await ElMessageBox.alert(result.msg);
      return false;
    } catch (error) {
      return false;
    }
  }


  // 默认实体
  function useDefaultModel() {
    return reactive({
      id: undefined,
      userId: userStore.getSession("id"),
      itemId: '',
      numbers: 0,
    })
  }

  return {
    cartList,
    getCartList,
    loading,
    addCart,
    useDefaultModel,
    updateItemNumber,
    removeCartById
  };
});
