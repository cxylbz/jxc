package com.spboot.app.pojo;
import java.io.Serializable;
public class ShoppingCartVo implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;

    // todo 用户id
    private String userId;

    // todo 商品id
    private String itemId;

    // todo 加入购物车商品数量
    private Integer numbers;

    // todo 商品信息  展示得时候取这个对象
    private Shangpin shangpin;

    public ShoppingCartVo(Integer id, String userId, String itemId, Integer numbers, Shangpin shangpin) {
        this.id = id;
        this.userId = userId;
        this.itemId = itemId;
        this.numbers = numbers;
        this.shangpin = shangpin;
    }

    public ShoppingCartVo() {

    }

    @Override
    public String toString() {
        return "ShoppingCartVo{" +
                "id=" + id +
                ", userId='" + userId + '\'' +
                ", itemId='" + itemId + '\'' +
                ", numbers=" + numbers +
                ", shangpin=" + shangpin +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public Integer getNumbers() {
        return numbers;
    }

    public void setNumbers(Integer numbers) {
        this.numbers = numbers;
    }

    public Shangpin getShangpin() {
        return shangpin;
    }

    public void setShangpin(Shangpin shangpin) {
        this.shangpin = shangpin;
    }
}
