import http from "@/utils/ajax/http";
import { useRoute } from "vue-router";
import { useUserStore } from "@/stores";
import { reactive, ref, unref } from "vue";
import rule from "@/utils/rule";
import { extend, isArray } from "@/utils/extend";
import { ElMessageBox } from "element-plus";
import router from "@/router";

import { canDingdanFindById } from "./dingdan";

/**
 * 响应式的对象数据
 */

export const TuihuanhuoCreateForm = () => {
    var route = unref(router.currentRoute);
    const userStore = useUserStore();
    const $session = userStore.session;
    if (!route.query) {
        route = useRoute();
    }
    const form = {
        bianhao: "",
        mingcheng: "",
        fenlei: "",
        jiage: "",
        tupian: "",
        shuliang: "",
        xingming: "",
        lianxifangshi: "",
        dizhi: "",
        tianjiaren: $session.username,
        leixing: "",
        tuihuanhuozhuangtai: "待审核",
        tuihuanmiaoshu: "",
        caozuoren: $session.username,
    };

    return form;
};

function exportForm(form, readMap) {
    var autoText = ["dingdanid", "shangpinid", "bianhao", "mingcheng", "fenlei", "jiage", "tupian", "shuliang", "zonge", "xingming", "lianxifangshi", "dizhi", "tianjiaren"];
    for (var txt of autoText) {
        form[txt] = readMap[txt];
    }
}

/**
 * 异步模式获取数据
 * @param id
 * @param readMap
 * @return {Promise}
 */
export const canTuihuanhuoCreateForm = (id, readMap) => {
    return new Promise(async (resolve, reject) => {
        var form = TuihuanhuoCreateForm();
        if (!readMap || !readMap.id) {
            readMap = await canDingdanFindById(id).catch(reject);
        }
        exportForm(form, readMap);
        form.dingdanid = readMap.id;
        resolve({ form, readMap });
    });
};

/**
 * 响应式获取退换货 模块的表单字段数据
 * @return {UnwrapNestedRefs<{}>}
 */
export const useTuihuanhuoCreateForm = (id) => {
    const form = TuihuanhuoCreateForm();
    const formReactive = reactive(form);

    const readMap = reactive({});
    canDingdanFindById(id).then(
        (map) => {
            exportForm(formReactive, map);
            extend(readMap, map);
            formReactive.dingdanid = map.id;
        },
        (err) => {
            ElMessageBox.alert(err.message);
        }
    );
    return { form: formReactive, readMap };
};

export const canTuihuanhuoSelect = (filter, result) => {
    http.post("/api/tuihuanhuo/selectPages").then((res) => {
        if (res.code == 0) {
            extend(result, res.data);
        } else {
            ElMessageBox.alert(res.msg);
        }
    });
};

/**
 * 获取分页数据
 * @param filter
 */
export const useTuihuanhuoSelect = (filter) => {
    const result = reactive({
        lists: [],
        total: {},
    });
    canTuihuanhuoSelect(filter, result);
    return result;
};

/**
 * 根据
 * @param id
 * @return {Promise|form}
 */
export const canTuihuanhuoFindById = (id) => {
    return new Promise((resolve, reject) => {
        // 读取后台数据
        http.get("/api/tuihuanhuo/findById", { id }).then((res) => {
            if (res.code == 0) {
                resolve(res.data);
            } else {
                reject(new Error(res.msg));
            }
        }, reject);
    });
};

/**
 * 根据id 获取一行数据
 * @param id
 * @return {UnwrapNestedRefs<{}>}
 */
export const useTuihuanhuoFindById = (id) => {
    var form = reactive({});

    canTuihuanhuoFindById(id).then((res) => {
        extend(form, res);
    });
    return form;
};

/**
 * 根据数据,插入到数据库中
 * @param data
 * @return {Promise<unknown>}
 */
export const canTuihuanhuoInsert = (data) => {
    return new Promise((resolve, reject) => {
        http.post("/api/tuihuanhuo/insert", data)
            .json()
            .then(
                (res) => {
                    resolve(res);
                },
                (err) => {
                    reject(err);
                }
            );
    });
};

/**
 * 根据数据更新数据库
 * @param data
 * @return {Promise<unknown>}
 */
export const canTuihuanhuoUpdate = (data) => {
    return new Promise((resolve, reject) => {
        http.post("/api/tuihuanhuo/update", data)
            .json()
            .then(
                (res) => {
                    resolve(res);
                },
                (err) => {
                    reject(err);
                }
            );
    });
};

/**
 * 根据id 或者列表id
 * @param id
 * @return {Promise<unknown>}
 */
export const canTuihuanhuoDelete = (id) => {
    var res = [];
    if (!isArray(id)) {
        res.push(id);
    } else {
        res = id;
    }

    return new Promise((resolve, reject) => {
        http.post("/api/tuihuanhuo/delete", res)
            .json()
            .then(
                (res) => {
                    resolve(res);
                },
                (err) => {
                    reject(err);
                }
            );
    });
};
