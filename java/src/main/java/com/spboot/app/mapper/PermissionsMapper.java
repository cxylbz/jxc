package com.spboot.app.mapper;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.spboot.app.pojo.Permissions;
import org.apache.ibatis.annotations.Mapper;

@Mapper
@TableName("权限")
public interface PermissionsMapper extends BaseMapper<Permissions> {}
