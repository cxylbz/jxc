package com.spboot.app.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jntoo.db.DB;
import java.io.Serializable;
import java.util.*;

@TableName("zhuanshangpin")
public class Zhuanshangpin implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer zengpinid;

    private String bianhao;

    private String mingcheng;

    private Integer fenlei;

    private Integer kucun;

    private String zhuanshangmiaoshu;

    private String zhuanshangzhuangtai;

    private String shenqingren;

    private String addtime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getZhuanshangshenheCount() {
        return DB.name("zhuanshangshenhe").where("zhuanshangpinid", id).count();
    }

    public Integer getZengpinid() {
        return zengpinid;
    }

    public void setZengpinid(Integer zengpinid) {
        this.zengpinid = zengpinid == null ? 0 : zengpinid;
    }

    public String getBianhao() {
        return bianhao;
    }

    public void setBianhao(String bianhao) {
        this.bianhao = bianhao == null ? "" : bianhao.trim();
    }

    public String getMingcheng() {
        return mingcheng;
    }

    public void setMingcheng(String mingcheng) {
        this.mingcheng = mingcheng == null ? "" : mingcheng.trim();
    }

    public Integer getFenlei() {
        return fenlei;
    }

    public void setFenlei(Integer fenlei) {
        this.fenlei = fenlei == null ? 0 : fenlei;
    }

    public Integer getKucun() {
        return kucun;
    }

    public void setKucun(Integer kucun) {
        this.kucun = kucun == null ? 0 : kucun;
    }

    public String getZhuanshangmiaoshu() {
        return zhuanshangmiaoshu;
    }

    public void setZhuanshangmiaoshu(String zhuanshangmiaoshu) {
        this.zhuanshangmiaoshu = zhuanshangmiaoshu == null ? "" : zhuanshangmiaoshu.trim();
    }

    public String getZhuanshangzhuangtai() {
        return zhuanshangzhuangtai;
    }

    public void setZhuanshangzhuangtai(String zhuanshangzhuangtai) {
        this.zhuanshangzhuangtai = zhuanshangzhuangtai == null ? "" : zhuanshangzhuangtai.trim();
    }

    public String getShenqingren() {
        return shenqingren;
    }

    public void setShenqingren(String shenqingren) {
        this.shenqingren = shenqingren == null ? "" : shenqingren.trim();
    }

    public String getAddtime() {
        return addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime == null ? "" : addtime.trim();
    }
}
