package com.spboot.app.mapper;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.spboot.app.pojo.Chuku;
import org.apache.ibatis.annotations.Mapper;

@Mapper
@TableName("出库")
public interface ChukuMapper extends BaseMapper<Chuku> {}
