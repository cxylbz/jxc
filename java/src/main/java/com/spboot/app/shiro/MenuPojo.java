package com.spboot.app.shiro;

import java.util.List;

public class MenuPojo {

    private String label;
    private String to;
    private List<MenuPojo> children;
    private String icon;

    public MenuPojo(String label, String to) {
        this.label = label;
        this.to = to;
    }

    public MenuPojo(String label, String to, List<MenuPojo> children, String icon) {
        this.label = label;
        this.to = to;
        this.children = children;
        this.icon = icon;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public List<MenuPojo> getChildren() {
        return children;
    }

    public void setChildren(List<MenuPojo> children) {
        this.children = children;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
