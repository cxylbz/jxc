package com.spboot.app.mapper;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.spboot.app.pojo.Shangpin;
import org.apache.ibatis.annotations.Mapper;

@Mapper
@TableName("商品")
public interface ShangpinMapper extends BaseMapper<Shangpin> {}
