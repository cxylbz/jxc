package com.spboot.app.shiro;

import com.jntoo.db.DB;
import com.jntoo.db.utils.Convert;
import com.jntoo.db.utils.StringUtil;
import com.spboot.app.pojo.*;
import com.spboot.app.service.*;
import com.spboot.app.utils.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class CustomRealm extends AuthorizingRealm {

    private static final Logger logger = LoggerFactory.getLogger(CustomRealm.class);

    @Resource
    private JwtTokenUtils jwtTokenUtils;

    @Autowired
    private YonghuService loginService;

    @Autowired
    private UserService userService;

    @Value("${admin.account}")
    private String adminAccount;

    /**
     * 必须重写此方法，不然Shiro会报错
     */
    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JwtToken;
    }

    /**
     * 为超级管理员添加所有权限
     * @param principals
     * @param permission
     * @return
     */
    @Override
    public boolean isPermitted(PrincipalCollection principals, String permission) {
        String userName = principals.getPrimaryPrincipal().toString();
        return adminAccount.equals(userName) || super.isPermitted(principals, permission);
    }

    /**
     * 为超级管理员添加所有角色
     * @param principals
     * @param roleIdentifier
     * @return
     */
    @Override
    public boolean hasRole(PrincipalCollection principals, String roleIdentifier) {
        String userName = principals.getPrimaryPrincipal().toString();
        return adminAccount.equals(userName) || super.hasRole(principals, roleIdentifier);
    }

    /**
     * @MethodName doGetAuthorizationInfo
     * @Description 权限配置类
     * @Param [principalCollection]
     * @Return AuthorizationInfo
     * @Author WangShiLin
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        logger.info("##################执行Shiro登录认证##################");
        //获取登录用户名
        //String name = (String) principalCollection.getPrimaryPrincipal();
        //查询用户名称
        Yonghu user = (Yonghu) principalCollection.getPrimaryPrincipal();
        if (user == null) {
            logger.error("授权失败，用户信息为空！！！");
            return null;
        }

        String name = user.getUsername();

        //添加角色和权限
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();

        if (name.equals(adminAccount)) {
            // 超级管理员登录
            simpleAuthorizationInfo.addRole("超级管理员");
            // 获取所有的权限路径
            List<Permissions> permissionsList = DB.name(Permissions.class).select();
            for (Permissions permissions : permissionsList) {
                simpleAuthorizationInfo.addStringPermission(permissions.getCode());
            }
        } else {
            List<Role> roleList = DB.name(Role.class).where("id", "in", user.getJuese()).select();
            for (Role role : roleList) {
                //添加角色
                simpleAuthorizationInfo.addRole(role.getRolename());
                //添加权限
                List<Permissions> permissionsList = DB.name(Permissions.class).where("id", "in", role.getPermissions()).select();
                for (Permissions permissions : permissionsList) {
                    simpleAuthorizationInfo.addStringPermission(permissions.getCode());
                }
            }
        }
        return simpleAuthorizationInfo;
    }

    /**
     * @MethodName doGetAuthenticationInfo
     * @Description 认证配置类
     * @Param [authenticationToken]
     * @Return AuthenticationInfo
     * @Author WangShiLin
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        if (StringUtils.isEmpty(authenticationToken.getPrincipal())) {
            return null;
        }

        String token = (String) authenticationToken.getCredentials(); // 校验token有效性
        R<Object> tokenLogin = userService.tokenLogin(token);
        int code = Convert.toInt(tokenLogin.get("code"));
        if (code == 0) {
            Map data = (Map) tokenLogin.get("data");
            Session session = (Session) data.get("session");
            Yonghu user = loginService.findByUsername(session.getUsername());
            if (user == null) {
                throw new AuthenticationException("用户不存在!");
            }
            return new SimpleAuthenticationInfo(user, token, getName());
        } else {
            throw new AuthenticationException("用户不存在!");
        }
    }
}
