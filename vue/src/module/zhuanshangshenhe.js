import http from "@/utils/ajax/http";
import { useRoute } from "vue-router";
import { useUserStore } from "@/stores";
import { reactive, ref, unref } from "vue";
import rule from "@/utils/rule";
import { extend, isArray } from "@/utils/extend";
import { ElMessageBox } from "element-plus";
import router from "@/router";

import { canZhuanshangpinFindById } from "./zhuanshangpin";

/**
 * 响应式的对象数据
 */

export const ZhuanshangshenheCreateForm = () => {
    var route = unref(router.currentRoute);
    const userStore = useUserStore();
    const $session = userStore.session;
    if (!route.query) {
        route = useRoute();
    }
    const form = {
        bianhao: "",
        mingcheng: "",
        fenlei: "",
        kucun: "",
        shenqingren: $session.username,
        shenhejieguo: "",
        shenhebeizhu: "",
        shenheren: $session.username,
    };

    return form;
};

function exportForm(form, readMap) {
    var autoText = ["zhuanshangpinid", "zengpinid", "bianhao", "mingcheng", "fenlei", "kucun", "shenqingren"];
    for (var txt of autoText) {
        form[txt] = readMap[txt];
    }
}

/**
 * 异步模式获取数据
 * @param id
 * @param readMap
 * @return {Promise}
 */
export const canZhuanshangshenheCreateForm = (id, readMap) => {
    return new Promise(async (resolve, reject) => {
        var form = ZhuanshangshenheCreateForm();
        if (!readMap || !readMap.id) {
            readMap = await canZhuanshangpinFindById(id).catch(reject);
        }
        exportForm(form, readMap);
        form.zhuanshangpinid = readMap.id;
        resolve({ form, readMap });
    });
};

/**
 * 响应式获取转商审核 模块的表单字段数据
 * @return {UnwrapNestedRefs<{}>}
 */
export const useZhuanshangshenheCreateForm = (id) => {
    const form = ZhuanshangshenheCreateForm();
    const formReactive = reactive(form);

    const readMap = reactive({});
    canZhuanshangpinFindById(id).then(
        (map) => {
            exportForm(formReactive, map);
            extend(readMap, map);
            formReactive.zhuanshangpinid = map.id;
        },
        (err) => {
            ElMessageBox.alert(err.message);
        }
    );
    return { form: formReactive, readMap };
};

export const canZhuanshangshenheSelect = (filter, result) => {
    http.post("/api/zhuanshangshenhe/selectPages").then((res) => {
        if (res.code == 0) {
            extend(result, res.data);
        } else {
            ElMessageBox.alert(res.msg);
        }
    });
};

/**
 * 获取分页数据
 * @param filter
 */
export const useZhuanshangshenheSelect = (filter) => {
    const result = reactive({
        lists: [],
        total: {},
    });
    canZhuanshangshenheSelect(filter, result);
    return result;
};

/**
 * 根据
 * @param id
 * @return {Promise|form}
 */
export const canZhuanshangshenheFindById = (id) => {
    return new Promise((resolve, reject) => {
        // 读取后台数据
        http.get("/api/zhuanshangshenhe/findById", { id }).then((res) => {
            if (res.code == 0) {
                resolve(res.data);
            } else {
                reject(new Error(res.msg));
            }
        }, reject);
    });
};

/**
 * 根据id 获取一行数据
 * @param id
 * @return {UnwrapNestedRefs<{}>}
 */
export const useZhuanshangshenheFindById = (id) => {
    var form = reactive({});

    canZhuanshangshenheFindById(id).then((res) => {
        extend(form, res);
    });
    return form;
};

/**
 * 根据数据,插入到数据库中
 * @param data
 * @return {Promise<unknown>}
 */
export const canZhuanshangshenheInsert = (data) => {
    return new Promise((resolve, reject) => {
        http.post("/api/zhuanshangshenhe/insert", data)
            .json()
            .then(
                (res) => {
                    resolve(res);
                },
                (err) => {
                    reject(err);
                }
            );
    });
};

/**
 * 根据数据更新数据库
 * @param data
 * @return {Promise<unknown>}
 */
export const canZhuanshangshenheUpdate = (data) => {
    return new Promise((resolve, reject) => {
        http.post("/api/zhuanshangshenhe/update", data)
            .json()
            .then(
                (res) => {
                    resolve(res);
                },
                (err) => {
                    reject(err);
                }
            );
    });
};

/**
 * 根据id 或者列表id
 * @param id
 * @return {Promise<unknown>}
 */
export const canZhuanshangshenheDelete = (id) => {
    var res = [];
    if (!isArray(id)) {
        res.push(id);
    } else {
        res = id;
    }

    return new Promise((resolve, reject) => {
        http.post("/api/zhuanshangshenhe/delete", res)
            .json()
            .then(
                (res) => {
                    resolve(res);
                },
                (err) => {
                    reject(err);
                }
            );
    });
};
