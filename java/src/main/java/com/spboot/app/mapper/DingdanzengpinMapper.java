package com.spboot.app.mapper;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.spboot.app.pojo.Dingdanzengpin;
import org.apache.ibatis.annotations.Mapper;

@Mapper
@TableName("订单赠品")
public interface DingdanzengpinMapper extends BaseMapper<Dingdanzengpin> {}
