import { defineStore } from "pinia";
import { ref, reactive, toRef } from "vue";
import http from "@/utils/ajax/http";
import { ElMessageBox } from "element-plus";

import { useUserStore } from "./user";

const useAddressApi = () => {
  const API_PREFIX = "/api/yonghu"

  const interceptors = (url, data, request) => {
    return new Promise((resolve, reject) => {
      request(`${API_PREFIX}${url}`, data).then(result => {
        if (result.code === 0) {
          resolve(result.data)
        } else {
          ElMessageBox.alert(res.msg);
          reject(new Error(result.msg))
        }
      }).catch(reject)
    })
  }

  return {
    post: (url, data) => interceptors(url, data, http.post),
    get: (url, data) => interceptors(url, data, http.get),
  }
}

export const useAddressStore = defineStore("address", () => {

  const userStore = useUserStore();

  const addressList = reactive([]);
  const loading = ref(false);

  const request = useAddressApi();

  async function getAddressList() {
    // loading.value = true;
    try {
      addressList.splice(0, addressList.length);
      const result = await request.post("/listUserAddress?userId=" + userStore.getSession("id"));
      addressList.push(...result);
    } finally {
      loading.loading = false;
    }
  }

  async function saveAddress(model) {

    const params = Object.entries(model).reduce((prev, [key, value]) => {
      prev.push(key + "=" + value);
      return prev;
    }, []).join("&");
    const result = await request.post("/saveUserAddress?" + params)
    return result.code === 0
  }

  async function getAddressInfoById(id) {
    if (!id && id !== 0) return null;
    try {
      return await request.post("/getUserAddressById?id=" + id)
    } catch (e) {
      return null
    }
  }

  async function updateAddressById(model) {
    if (!model.id && model.id !== 0) return null;

    const params = Object.entries(model).reduce((prev, [key, value]) => {
      prev.push(key + "=" + value);
      return prev;
    }, []).join("&");

    try {
      return await request.post("/updateUserAddress?" + params)
    } catch (e) {
      return null
    }
  }

  async function removeAddressById(id) {
    try {
      await request.post("/delUserAddressById?id=" + id)
      return result.code === 0
    } catch (e) {
      return false;
    }
  }

  // 默认实体
  function useDefaultModel() {
    return reactive({
      id: undefined,
      names: '',
      isDefault: false,
      userId: userStore.getSession("id"),
      address: '',
      mobile: '',
    })
  }

  return {
    addressList,
    loading,
    getAddressList,
    getAddressInfoById,
    updateAddressById,
    saveAddress,
    removeAddressById,
    useDefaultModel
  };
});
