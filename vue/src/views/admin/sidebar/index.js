import { useUserStore } from "@/stores/user";
import http from "@/utils/ajax/http";

function formatMenus(lists) {
    for (var ci of lists) {
        if (ci.to.indexOf("{") === 0) {
            ci.to = eval("(" + ci.to + ")");
        }
        if (ci.children) {
            formatMenus(ci.children);
        }
    }
}

export function getMenus() {
    return new Promise((resolve, reject) => {
        http.get("/api/user/menus").then((res) => {
            if (res.code === 0) {
                formatMenus(res.data);
                resolve(res.data);
            } else {
                reject(new Error(res.msg));
            }
        }, reject);
    });
}

export default {
    getMenus,
};
