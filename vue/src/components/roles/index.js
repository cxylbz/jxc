import roles from "@/components/roles/roles";
import role from "@/components/roles/role";

export default function install(Vue) {
    Vue.component(roles.name, roles);
    Vue.component(role.name, role);
}
