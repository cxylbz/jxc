package com.spboot.app.mapper;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.spboot.app.pojo.Tuihuanshenhe;
import org.apache.ibatis.annotations.Mapper;

@Mapper
@TableName("退换审核")
public interface TuihuanshenheMapper extends BaseMapper<Tuihuanshenhe> {}
