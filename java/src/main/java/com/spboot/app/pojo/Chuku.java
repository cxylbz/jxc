package com.spboot.app.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jntoo.db.DB;
import java.io.Serializable;
import java.util.*;

@TableName("chuku")
public class Chuku implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer dingdanid;

    private Integer shangpinid;

    private String bianhao;

    private String mingcheng;

    private Integer fenlei;

    private String tupian;

    private Double jiage;

    private Integer shuliang;

    private Double zonge;

    private String xingming;

    private String lianxifangshi;

    private String dizhi;

    private String tianjiaren;

    private String kuaidigongsi;

    private String kuaididanhao;

    private String chukubeizhu;

    private String caozuoren;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDingdanid() {
        return dingdanid;
    }

    public void setDingdanid(Integer dingdanid) {
        this.dingdanid = dingdanid == null ? 0 : dingdanid;
    }

    public Integer getShangpinid() {
        return shangpinid;
    }

    public void setShangpinid(Integer shangpinid) {
        this.shangpinid = shangpinid == null ? 0 : shangpinid;
    }

    public String getBianhao() {
        return bianhao;
    }

    public void setBianhao(String bianhao) {
        this.bianhao = bianhao == null ? "" : bianhao.trim();
    }

    public String getMingcheng() {
        return mingcheng;
    }

    public void setMingcheng(String mingcheng) {
        this.mingcheng = mingcheng == null ? "" : mingcheng.trim();
    }

    public Integer getFenlei() {
        return fenlei;
    }

    public void setFenlei(Integer fenlei) {
        this.fenlei = fenlei == null ? 0 : fenlei;
    }

    public String getTupian() {
        return tupian;
    }

    public void setTupian(String tupian) {
        this.tupian = tupian == null ? "" : tupian.trim();
    }

    public Double getJiage() {
        return jiage;
    }

    public void setJiage(Double jiage) {
        this.jiage = jiage == null ? 0.0f : jiage;
    }

    public Integer getShuliang() {
        return shuliang;
    }

    public void setShuliang(Integer shuliang) {
        this.shuliang = shuliang == null ? 0 : shuliang;
    }

    public Double getZonge() {
        return zonge;
    }

    public void setZonge(Double zonge) {
        this.zonge = zonge == null ? 0.0f : zonge;
    }

    public String getXingming() {
        return xingming;
    }

    public void setXingming(String xingming) {
        this.xingming = xingming == null ? "" : xingming.trim();
    }

    public String getLianxifangshi() {
        return lianxifangshi;
    }

    public void setLianxifangshi(String lianxifangshi) {
        this.lianxifangshi = lianxifangshi == null ? "" : lianxifangshi.trim();
    }

    public String getDizhi() {
        return dizhi;
    }

    public void setDizhi(String dizhi) {
        this.dizhi = dizhi == null ? "" : dizhi.trim();
    }

    public String getTianjiaren() {
        return tianjiaren;
    }

    public void setTianjiaren(String tianjiaren) {
        this.tianjiaren = tianjiaren == null ? "" : tianjiaren.trim();
    }

    public String getKuaidigongsi() {
        return kuaidigongsi;
    }

    public void setKuaidigongsi(String kuaidigongsi) {
        this.kuaidigongsi = kuaidigongsi == null ? "" : kuaidigongsi.trim();
    }

    public String getKuaididanhao() {
        return kuaididanhao;
    }

    public void setKuaididanhao(String kuaididanhao) {
        this.kuaididanhao = kuaididanhao == null ? "" : kuaididanhao.trim();
    }

    public String getChukubeizhu() {
        return chukubeizhu;
    }

    public void setChukubeizhu(String chukubeizhu) {
        this.chukubeizhu = chukubeizhu == null ? "" : chukubeizhu.trim();
    }

    public String getCaozuoren() {
        return caozuoren;
    }

    public void setCaozuoren(String caozuoren) {
        this.caozuoren = caozuoren == null ? "" : caozuoren.trim();
    }
}
