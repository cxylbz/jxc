package com.spboot.app.shiro;

import org.springframework.http.HttpStatus;

public class CustomException extends RuntimeException {

    public CustomException(String s, HttpStatus unauthorized) {
        super(s);
    }
}
