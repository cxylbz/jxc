import { session } from "@/utils/utils";

export default [
    {
        path: "pay",
        name: "AdminPay",
        component: () => import("@/views/zhifu/zhifu.vue"),
        meta: { authLogin: true },
    },

    {
        path: "yonghu",
        name: "AdminyonghuList",
        component: () => import("@/views/yonghu/list.vue"),
        meta: { title: "用户列表", authLogin: true },
    },

    {
        path: "yonghu/add",
        name: "AdminyonghuAdd",
        component: () => import("@/views/yonghu/add.vue"),
        meta: { title: "添加用户", authLogin: true },
    },
    {
        path: "yonghu/updt",
        name: "AdminyonghuUpdt",
        props: (route) => ({ id: route.query.id }),
        component: () => import("@/views/yonghu/updt.vue"),
        meta: { title: "编辑用户", authLogin: true },
    },
    {
        path: "yonghu/updtself",
        name: "AdminyonghuUpdtSelf",
        props: (route) => ({ id: session("id") }),
        component: () => import("@/views/yonghu/updtself.vue"),
        meta: { title: "更新个人资料", authLogin: true },
    },
    {
        path: "role",
        name: "AdminroleList",
        component: () => import("@/views/role/list.vue"),
        meta: { title: "角色列表", authLogin: true },
    },

    {
        path: "role/add",
        name: "AdminroleAdd",
        component: () => import("@/views/role/add.vue"),
        meta: { title: "添加角色", authLogin: true },
    },
    {
        path: "role/updt",
        name: "AdminroleUpdt",
        props: (route) => ({ id: route.query.id }),
        component: () => import("@/views/role/updt.vue"),
        meta: { title: "编辑角色", authLogin: true },
    },
    {
        path: "permissions",
        name: "AdminpermissionsList",
        component: () => import("@/views/permissions/list.vue"),
        meta: { title: "权限列表", authLogin: true },
    },

    {
        path: "permissions/add",
        name: "AdminpermissionsAdd",
        component: () => import("@/views/permissions/add.vue"),
        meta: { title: "添加权限", authLogin: true },
    },
    {
        path: "permissions/updt",
        name: "AdminpermissionsUpdt",
        props: (route) => ({ id: route.query.id }),
        component: () => import("@/views/permissions/updt.vue"),
        meta: { title: "编辑权限", authLogin: true },
    },
    {
        path: "fenlei",
        name: "AdminfenleiList",
        component: () => import("@/views/fenlei/list.vue"),
        meta: { title: "分类列表", authLogin: true },
    },

    {
        path: "fenlei/add",
        name: "AdminfenleiAdd",
        component: () => import("@/views/fenlei/add.vue"),
        meta: { title: "添加分类", authLogin: true },
    },
    {
        path: "fenlei/updt",
        name: "AdminfenleiUpdt",
        props: (route) => ({ id: route.query.id }),
        component: () => import("@/views/fenlei/updt.vue"),
        meta: { title: "编辑分类", authLogin: true },
    },
    {
        path: "shangpin",
        name: "AdminshangpinList",
        component: () => import("@/views/shangpin/list.vue"),
        meta: { title: "商品列表", authLogin: true },
    },


    {
        path: "shangpin/tianjiaren",
        name: "AdminshangpinListtianjiaren",
        component: () => import("@/views/shangpin/tianjiaren.vue"),
        meta: { title: "商品列表", authLogin: true },
    },

    {
        path: "shangpin/add",
        name: "AdminshangpinAdd",
        component: () => import("@/views/shangpin/add.vue"),
        meta: { title: "添加商品", authLogin: true },
    },
    {
        path: "shangpin/updt",
        name: "AdminshangpinUpdt",
        props: (route) => ({ id: route.query.id }),
        component: () => import("@/views/shangpin/updt.vue"),
        meta: { title: "编辑商品", authLogin: true },
    },
    {
        path: "shangpin/detail",
        props: (route) => ({ id: route.query.id }),
        name: "AdminshangpinDetail",
        component: () => import("@/views/shangpin/detail.vue"),
        meta: { title: "商品详情", authLogin: true },
    },
    {
        path: "zengpin",
        name: "AdminzengpinList",
        component: () => import("@/views/zengpin/list.vue"),
        meta: { title: "赠品列表", authLogin: true },
    },

    {
        path: "zengpin/tianjiaren",
        name: "AdminzengpinListtianjiaren",
        component: () => import("@/views/zengpin/tianjiaren.vue"),
        meta: { title: "赠品列表", authLogin: true },
    },

    {
        path: "zengpin/add",
        name: "AdminzengpinAdd",
        component: () => import("@/views/zengpin/add.vue"),
        meta: { title: "添加赠品", authLogin: true },
    },
    {
        path: "zengpin/updt",
        name: "AdminzengpinUpdt",
        props: (route) => ({ id: route.query.id }),
        component: () => import("@/views/zengpin/updt.vue"),
        meta: { title: "编辑赠品", authLogin: true },
    },
    {
        path: "zengpin/detail",
        props: (route) => ({ id: route.query.id }),
        name: "AdminzengpinDetail",
        component: () => import("@/views/zengpin/detail.vue"),
        meta: { title: "赠品详情", authLogin: true },
    },
    {
        path: "dingdan",
        name: "AdmindingdanList",
        component: () => import("@/views/dingdan/list.vue"),
        meta: { title: "订单列表", authLogin: true },
    },

    {
        path: "dingdan/tianjiaren",
        name: "AdmindingdanListtianjiaren",
        component: () => import("@/views/dingdan/tianjiaren.vue"),
        meta: { title: "订单列表", authLogin: true },
    },

    {
        path: "dingdan/add",
        name: "AdmindingdanAdd",
        props: (route) => ({ id: route.query.id }),
        component: () => import("@/views/dingdan/add.vue"),
        meta: { title: "添加订单", authLogin: true },
    },
    {
        path: "dingdan/updt",
        name: "AdmindingdanUpdt",
        props: (route) => ({ id: route.query.id }),
        component: () => import("@/views/dingdan/updt.vue"),
        meta: { title: "编辑订单", authLogin: true },
    },
    {
        path: "dingdan/detail",
        props: (route) => ({ id: route.query.id }),
        name: "AdmindingdanDetail",
        component: () => import("@/views/dingdan/detail.vue"),
        meta: { title: "订单详情", authLogin: true },
    },
    {
        path: "dingdanzengpin",
        name: "AdmindingdanzengpinList",
        component: () => import("@/views/dingdanzengpin/list.vue"),
        meta: { title: "订单赠品列表", authLogin: true },
    },

    {
        path: "dingdanzengpin/tianjiaren",
        name: "AdmindingdanzengpinListtianjiaren",
        component: () => import("@/views/dingdanzengpin/tianjiaren.vue"),
        meta: { title: "订单赠品列表", authLogin: true },
    },

    {
        path: "dingdanzengpin/add",
        name: "AdmindingdanzengpinAdd",
        component: () => import("@/views/dingdanzengpin/add.vue"),
        meta: { title: "添加订单赠品", authLogin: true },
    },
    {
        path: "dingdanzengpin/updt",
        name: "AdmindingdanzengpinUpdt",
        props: (route) => ({ id: route.query.id }),
        component: () => import("@/views/dingdanzengpin/updt.vue"),
        meta: { title: "编辑订单赠品", authLogin: true },
    },
    {
        path: "dingdanzengpin/detail",
        props: (route) => ({ id: route.query.id }),
        name: "AdmindingdanzengpinDetail",
        component: () => import("@/views/dingdanzengpin/detail.vue"),
        meta: { title: "订单赠品详情", authLogin: true },
    },
    {
        path: "zhuanshangpin",
        name: "AdminzhuanshangpinList",
        component: () => import("@/views/zhuanshangpin/list.vue"),
        meta: { title: "转商品列表", authLogin: true },
    },

    {
        path: "zhuanshangpin/shenqingren",
        name: "AdminzhuanshangpinListshenqingren",
        component: () => import("@/views/zhuanshangpin/shenqingren.vue"),
        meta: { title: "转商品列表", authLogin: true },
    },

    {
        path: "zhuanshangpin/add",
        name: "AdminzhuanshangpinAdd",
        props: (route) => ({ id: route.query.id }),
        component: () => import("@/views/zhuanshangpin/add.vue"),
        meta: { title: "添加转商品", authLogin: true },
    },
    {
        path: "zhuanshangpin/updt",
        name: "AdminzhuanshangpinUpdt",
        props: (route) => ({ id: route.query.id }),
        component: () => import("@/views/zhuanshangpin/updt.vue"),
        meta: { title: "编辑转商品", authLogin: true },
    },
    {
        path: "zhuanshangpin/detail",
        props: (route) => ({ id: route.query.id }),
        name: "AdminzhuanshangpinDetail",
        component: () => import("@/views/zhuanshangpin/detail.vue"),
        meta: { title: "转商品详情", authLogin: true },
    },
    {
        path: "zhuanshangshenhe",
        name: "AdminzhuanshangshenheList",
        component: () => import("@/views/zhuanshangshenhe/list.vue"),
        meta: { title: "转商审核列表", authLogin: true },
    },

    {
        path: "zhuanshangshenhe/shenqingren",
        name: "AdminzhuanshangshenheListshenqingren",
        component: () => import("@/views/zhuanshangshenhe/shenqingren.vue"),
        meta: { title: "转商审核列表", authLogin: true },
    },
    {
        path: "zhuanshangshenhe/shenheren",
        name: "AdminzhuanshangshenheListshenheren",
        component: () => import("@/views/zhuanshangshenhe/shenheren.vue"),
        meta: { title: "转商审核列表", authLogin: true },
    },

    {
        path: "zhuanshangshenhe/add",
        name: "AdminzhuanshangshenheAdd",
        props: (route) => ({ id: route.query.id }),
        component: () => import("@/views/zhuanshangshenhe/add.vue"),
        meta: { title: "添加转商审核", authLogin: true },
    },
    {
        path: "zhuanshangshenhe/updt",
        name: "AdminzhuanshangshenheUpdt",
        props: (route) => ({ id: route.query.id }),
        component: () => import("@/views/zhuanshangshenhe/updt.vue"),
        meta: { title: "编辑转商审核", authLogin: true },
    },
    {
        path: "zhuanshangshenhe/detail",
        props: (route) => ({ id: route.query.id }),
        name: "AdminzhuanshangshenheDetail",
        component: () => import("@/views/zhuanshangshenhe/detail.vue"),
        meta: { title: "转商审核详情", authLogin: true },
    },
    {
        path: "zhuanzengpin",
        name: "AdminzhuanzengpinList",
        component: () => import("@/views/zhuanzengpin/list.vue"),
        meta: { title: "转赠品列表", authLogin: true },
    },

    {
        path: "zhuanzengpin/shenqingren",
        name: "AdminzhuanzengpinListshenqingren",
        component: () => import("@/views/zhuanzengpin/shenqingren.vue"),
        meta: { title: "转赠品列表", authLogin: true },
    },

    {
        path: "zhuanzengpin/add",
        name: "AdminzhuanzengpinAdd",
        props: (route) => ({ id: route.query.id }),
        component: () => import("@/views/zhuanzengpin/add.vue"),
        meta: { title: "添加转赠品", authLogin: true },
    },
    {
        path: "zhuanzengpin/updt",
        name: "AdminzhuanzengpinUpdt",
        props: (route) => ({ id: route.query.id }),
        component: () => import("@/views/zhuanzengpin/updt.vue"),
        meta: { title: "编辑转赠品", authLogin: true },
    },
    {
        path: "zhuanzengpin/detail",
        props: (route) => ({ id: route.query.id }),
        name: "AdminzhuanzengpinDetail",
        component: () => import("@/views/zhuanzengpin/detail.vue"),
        meta: { title: "转赠品详情", authLogin: true },
    },
    {
        path: "zhuanzengshenhe",
        name: "AdminzhuanzengshenheList",
        component: () => import("@/views/zhuanzengshenhe/list.vue"),
        meta: { title: "转赠审核列表", authLogin: true },
    },

    {
        path: "zhuanzengshenhe/shenqingren",
        name: "AdminzhuanzengshenheListshenqingren",
        component: () => import("@/views/zhuanzengshenhe/shenqingren.vue"),
        meta: { title: "转赠审核列表", authLogin: true },
    },

    {
        path: "zhuanzengshenhe/add",
        name: "AdminzhuanzengshenheAdd",
        props: (route) => ({ id: route.query.id }),
        component: () => import("@/views/zhuanzengshenhe/add.vue"),
        meta: { title: "添加转赠审核", authLogin: true },
    },
    {
        path: "zhuanzengshenhe/updt",
        name: "AdminzhuanzengshenheUpdt",
        props: (route) => ({ id: route.query.id }),
        component: () => import("@/views/zhuanzengshenhe/updt.vue"),
        meta: { title: "编辑转赠审核", authLogin: true },
    },
    {
        path: "zhuanzengshenhe/detail",
        props: (route) => ({ id: route.query.id }),
        name: "AdminzhuanzengshenheDetail",
        component: () => import("@/views/zhuanzengshenhe/detail.vue"),
        meta: { title: "转赠审核详情", authLogin: true },
    },
    {
        path: "chuku",
        name: "AdminchukuList",
        component: () => import("@/views/chuku/list.vue"),
        meta: { title: "出库列表", authLogin: true },
    },

    {
        path: "chuku/tianjiaren",
        name: "AdminchukuListtianjiaren",
        component: () => import("@/views/chuku/tianjiaren.vue"),
        meta: { title: "出库列表", authLogin: true },
    },

    {
        path: "chuku/add",
        name: "AdminchukuAdd",
        props: (route) => ({ id: route.query.id }),
        component: () => import("@/views/chuku/add.vue"),
        meta: { title: "添加出库", authLogin: true },
    },
    {
        path: "chuku/updt",
        name: "AdminchukuUpdt",
        props: (route) => ({ id: route.query.id }),
        component: () => import("@/views/chuku/updt.vue"),
        meta: { title: "编辑出库", authLogin: true },
    },
    {
        path: "chuku/detail",
        props: (route) => ({ id: route.query.id }),
        name: "AdminchukuDetail",
        component: () => import("@/views/chuku/detail.vue"),
        meta: { title: "出库详情", authLogin: true },
    },
    {
        path: "tuihuanhuo",
        name: "AdmintuihuanhuoList",
        component: () => import("@/views/tuihuanhuo/list.vue"),
        meta: { title: "退换货列表", authLogin: true },
    },

    {
        path: "tuihuanhuo/tianjiaren",
        name: "AdmintuihuanhuoListtianjiaren",
        component: () => import("@/views/tuihuanhuo/tianjiaren.vue"),
        meta: { title: "退换货列表", authLogin: true },
    },
    {
        path: "tuihuanhuo/caozuoren",
        name: "AdmintuihuanhuoListcaozuoren",
        component: () => import("@/views/tuihuanhuo/caozuoren.vue"),
        meta: { title: "退换货列表", authLogin: true },
    },

    {
        path: "tuihuanhuo/add",
        name: "AdmintuihuanhuoAdd",
        props: (route) => ({ id: route.query.id }),
        component: () => import("@/views/tuihuanhuo/add.vue"),
        meta: { title: "添加退换货", authLogin: true },
    },
    {
        path: "tuihuanhuo/updt",
        name: "AdmintuihuanhuoUpdt",
        props: (route) => ({ id: route.query.id }),
        component: () => import("@/views/tuihuanhuo/updt.vue"),
        meta: { title: "编辑退换货", authLogin: true },
    },
    {
        path: "tuihuanhuo/detail",
        props: (route) => ({ id: route.query.id }),
        name: "AdmintuihuanhuoDetail",
        component: () => import("@/views/tuihuanhuo/detail.vue"),
        meta: { title: "退换货详情", authLogin: true },
    },
    {
        path: "tuihuanshenhe",
        name: "AdmintuihuanshenheList",
        component: () => import("@/views/tuihuanshenhe/list.vue"),
        meta: { title: "退换审核列表", authLogin: true },
    },

    {
        path: "tuihuanshenhe/caozuoren",
        name: "AdmintuihuanshenheListcaozuoren",
        component: () => import("@/views/tuihuanshenhe/caozuoren.vue"),
        meta: { title: "退换审核列表", authLogin: true },
    },
    {
        path: "tuihuanshenhe/tianjiaren",
        name: "AdmintuihuanshenheListtianjiaren",
        component: () => import("@/views/tuihuanshenhe/tianjiaren.vue"),
        meta: { title: "退换审核列表", authLogin: true },
    },
    {
        path: "tuihuanshenhe/shenheren",
        name: "AdmintuihuanshenheListshenheren",
        component: () => import("@/views/tuihuanshenhe/shenheren.vue"),
        meta: { title: "退换审核列表", authLogin: true },
    },

    {
        path: "tuihuanshenhe/add",
        name: "AdmintuihuanshenheAdd",
        props: (route) => ({ id: route.query.id }),
        component: () => import("@/views/tuihuanshenhe/add.vue"),
        meta: { title: "添加退换审核", authLogin: true },
    },
    {
        path: "tuihuanshenhe/updt",
        name: "AdmintuihuanshenheUpdt",
        props: (route) => ({ id: route.query.id }),
        component: () => import("@/views/tuihuanshenhe/updt.vue"),
        meta: { title: "编辑退换审核", authLogin: true },
    },
    {
        path: "tuihuanshenhe/detail",
        props: (route) => ({ id: route.query.id }),
        name: "AdmintuihuanshenheDetail",
        component: () => import("@/views/tuihuanshenhe/detail.vue"),
        meta: { title: "退换审核详情", authLogin: true },
    },
    {
        path: "ruku",
        name: "AdminrukuList",
        component: () => import("@/views/ruku/list.vue"),
        meta: { title: "入库列表", authLogin: true },
    },

    {
        path: "ruku/tianjiaren",
        name: "AdminrukuListtianjiaren",
        component: () => import("@/views/ruku/tianjiaren.vue"),
        meta: { title: "入库列表", authLogin: true },
    },
    {
        path: "ruku/caozuoren",
        name: "AdminrukuListcaozuoren",
        component: () => import("@/views/ruku/caozuoren.vue"),
        meta: { title: "入库列表", authLogin: true },
    },

    {
        path: "ruku/add",
        name: "AdminrukuAdd",
        props: (route) => ({ id: route.query.id }),
        component: () => import("@/views/ruku/add.vue"),
        meta: { title: "添加入库", authLogin: true },
    },
    {
        path: "ruku/updt",
        name: "AdminrukuUpdt",
        props: (route) => ({ id: route.query.id }),
        component: () => import("@/views/ruku/updt.vue"),
        meta: { title: "编辑入库", authLogin: true },
    },
    {
        path: "ruku/detail",
        props: (route) => ({ id: route.query.id }),
        name: "AdminrukuDetail",
        component: () => import("@/views/ruku/detail.vue"),
        meta: { title: "入库详情", authLogin: true },
    },
    {
        path: "lunbotu",
        name: "AdminlunbotuList",
        component: () => import("@/views/lunbotu/list.vue"),
        meta: { title: "轮播图列表", authLogin: true },
    },

    {
        path: "lunbotu/add",
        name: "AdminlunbotuAdd",
        component: () => import("@/views/lunbotu/add.vue"),
        meta: { title: "添加轮播图", authLogin: true },
    },
    {
        path: "lunbotu/updt",
        name: "AdminlunbotuUpdt",
        props: (route) => ({ id: route.query.id }),
        component: () => import("@/views/lunbotu/updt.vue"),
        meta: { title: "编辑轮播图", authLogin: true },
    },
];
