import http from "@/utils/ajax/http";
import { useRoute } from "vue-router";
import { session } from "@/utils/utils";
import { reactive } from "vue";
import rule from "@/utils/rule";
import { extend, isArray } from "@/utils/extend";
import { ElMessageBox } from "element-plus";

/**
 * 响应式的对象数据
 */

/**
 * 获取admins 模块的表单字段数据
 * @return {UnwrapNestedRefs<{}>}
 */
export const useAdminsCreateForm = () => {
    const route = useRoute();
    const form = {};
    const formReactive = reactive({});
    return formReactive;
};

/**
 * 获取分页数据
 * @param filter
 */
export const useAdminsSelect = (filter) => {
    const result = reactive({
        lists: [],
        total: {},
    });
    http.post("/api/admin/selectPages").then((res) => {
        if (res.code == 0) {
            extend(result, res.data);
        } else {
            ElMessageBox.alert(res.msg);
        }
    });
    return result;
};
/**
 * 根据id 获取一行数据
 * @param id
 * @return {UnwrapNestedRefs<{}>}
 */
export const useAdminsFindById = (id) => {
    var form = useAdminsCreateForm();

    // 读取后台数据
    http.get("/api/admins/findById", { id }).then((res) => {
        if (res.code == 0) {
            extend(form, res.data);
        } else {
            ElMessageBox.alert(res.msg);
        }
    });

    return form;
};

/**
 * 根据数据,插入到数据库中
 * @param data
 * @return {Promise<unknown>}
 */
export const canAdminsInsert = (data) => {
    return new Promise((resolve, reject) => {
        http.post("/api/<?php echo $moduleName ?>/insert", data)
            .json()
            .then(
                (res) => {
                    resolve(res);
                },
                (err) => {
                    reject(err);
                }
            );
    });
};

/**
 * 根据数据更新数据库
 * @param data
 * @return {Promise<unknown>}
 */
export const canAdminsUpdate = (data) => {
    return new Promise((resolve, reject) => {
        http.post("/api/<?php echo $moduleName ?>/update", data)
            .json()
            .then(
                (res) => {
                    resolve(res);
                },
                (err) => {
                    reject(err);
                }
            );
    });
};

/**
 * 根据id 或者列表id
 * @param id
 * @return {Promise<unknown>}
 */
export const canAdminsDelete = (id) => {
    var res = [];
    if (!isArray(id)) {
        res.push(id);
    } else {
        res = id;
    }

    return new Promise((resolve, reject) => {
        http.post("/api/<?php echo $moduleName ?>/delete", res)
            .json()
            .then(
                (res) => {
                    resolve(res);
                },
                (err) => {
                    reject(err);
                }
            );
    });
};
